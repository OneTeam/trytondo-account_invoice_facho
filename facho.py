from trytond.model import fields, ModelSQL, ModelView


class FEGenericCode(ModelSQL, ModelView):
    "FE GenericCode"
    __name__ = 'account_invoice_facho.fe_generic_code'

    resource = fields.Selection([
        ('tipo_responsabilidad', 'Tipo Responsabilidad'),
        ('tipo_organizacion', 'Tipo Organizacion')
    ], 'Resource')
    name = fields.Char('Name')
    code = fields.Char('Code')


