try:
    from trytond.modules.account_invoice_facho.tests.test_account_invoice_facho import suite  # noqa: E501
except ImportError:
    from .test_account_invoice_facho import suite

__all__ = ['suite']
