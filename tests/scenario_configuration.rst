==============================
Account Invoice Facho Scenario
==============================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company

Install account_invoice_facho::

    >>> config = activate_modules('account_invoice_facho')

Create company::

    >>> _ = create_company()
    >>> company = get_company()
