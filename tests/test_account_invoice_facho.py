import unittest

import doctest

from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite
from trytond.tests.test_tryton import doctest_teardown
from trytond.tests.test_tryton import doctest_checker
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.modules.company.tests import create_company, set_company
from trytond.modules.account.tests import create_chart

class AccountInvoiceFachoTestCase(ModuleTestCase):
    'Test Account Invoice Facho module'
    module = 'account_invoice_facho'

            
def suite():
    suite = test_suite()
    #suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
    #        AccountInvoiceFachoTestCase))
    suite.addTests(doctest.DocFileSuite(
            'scenario_account_invoice_facho.rst',
            tearDown=doctest_teardown, encoding='utf-8',
            checker=doctest_checker,
            optionflags=doctest.REPORT_ONLY_FIRST_FAILURE))
    suite.addTests(doctest.DocFileSuite(
            'scenario_configuration.rst',
            tearDown=doctest_teardown, encoding='utf-8',
            checker=doctest_checker,
            optionflags=doctest.REPORT_ONLY_FIRST_FAILURE))
    return suite
