from trytond.pool import Pool
from trytond.model import ModelSQL, ModelView, ModelSingleton, fields
from trytond.modules.company.model import (
    CompanyMultiValueMixin, CompanyValueMixin)


class Configuration(
        ModelSingleton, ModelSQL, ModelView, CompanyMultiValueMixin):
    'Facho Configuration'
    __name__ = 'account_invoice_facho.configuration'

    dian_fe_habilitacion = fields.MultiValue(fields.Boolean('Habilitacion'))
    dian_fe_key= fields.MultiValue(fields.Char('Dian FE KEY'))
    dian_fe_llave_publica = fields.MultiValue(fields.Char('Publica PCKS#12'))
    dian_fe_llave_privada = fields.MultiValue(fields.Char('Privada PCKS#12'))
    dian_fe_llave_frasepaso = fields.MultiValue(fields.Char('Frasepaso PCKS#12'))
    dian_fe_test_setid = fields.MultiValue(fields.Char('Dian testsetid'))
    dian_fe_NITProveedorTecnologico = fields.MultiValue(fields.Char('NIT Proveedor Tecnologico'))
    dian_fe_NITObligadoFacturarElectronicamente = fields.MultiValue(fields.Char('NIT Obligactior FE'))
    dian_fe_IdentificadorSoftware = fields.MultiValue(fields.Char('IdentificadorSoftware'))
    
    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        return pool.get('account_invoice_facho.dian_fe_company')


class ConfigurationDianFECompany(ModelSQL, CompanyValueMixin):
    'Configuration - DianFECompany'
    __name__ = 'account_invoice_facho.dian_fe_company'

    configuration = fields.Many2One('account_invoice_facho.configuration', 'Configuration',
        required=True, ondelete='CASCADE')

    dian_fe_habilitacion = fields.Boolean('Habilitacion')
    dian_fe_key = fields.Char('Dian key')
    dian_fe_llave_publica = fields.Char('Publica PCKS#12')
    dian_fe_llave_privada = fields.Char('Publica PCKS#12')
    dian_fe_test_setid = fields.Char('Dian testsetid')
    dian_fe_llave_frasepaso = fields.Char('Frasepaso PCKS#12')
    dian_fe_NITProveedorTecnologico = fields.Char('NIT Proveedor Tecnologico')
    dian_fe_NITObligadoFacturarElectronicamente = fields.Char('NIT Obligactior FE')
    dian_fe_IdentificadorSoftware = fields.Char('IdentificadorSoftware')
