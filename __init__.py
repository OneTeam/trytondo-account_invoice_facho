from trytond.pool import Pool
from . import configuration

from . import invoice
from . import facho
from . import dian

__all__ = ['register']


def register():
    Pool.register(
        configuration.Configuration,
        configuration.ConfigurationDianFECompany,
        facho.FEGenericCode,
        invoice.Invoice,
        invoice.InvoiceLine,
        invoice.Product,
        invoice.PartyFeIdentifier,
        invoice.PartyFeOrganization,
        invoice.Party,
        invoice.Cron,
        dian.RangoFacturacion,
        module='account_invoice_facho', type_='model')
    Pool.register(
        dian.SincronizarRangoFacturacion,
        module='account_invoice_facho', type_='wizard')
    Pool.register(
        module='account_invoice_facho', type_='report')
