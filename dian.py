from trytond.model import ModelSQL, ModelView, fields
from trytond.wizard import Wizard, StateTransition
from trytond.pool import Pool


class RangoFacturacion(ModelSQL, ModelView):
    'Rango Facturacion'
    __name__ = 'account_invoice_facho.dian_rango_facturacion'

    NumeroResolucion = fields.Char('Numero Resolucion')
    FechaResolucion = fields.Date('Fecha Resolucion')
    Prefijo = fields.Char('Prefijo')
    RangoInicial = fields.Integer('Rango Inicial')
    RangoFinal = fields.Integer('Rango Final')
    FechaVigenciaDesde = fields.Date('Fecha Vigencia Desde')
    FechaVigenciaHasta = fields.Date('Fecha Vigencia Hasta')
    ClaveTecnica = fields.Char('Clave Tecnica')



class SincronizarRangoFacturacion(Wizard):
    'Sincronizar Rango Facturacion'
    __name__ = 'account_invoice_facho.sincronizar_rango_facturacion'
    start = StateTransition()

    # TODO aun no esta PROBADO!!!
    def transition_start(self):
        pool = Pool()
        config = pool.get('account_invoice_facho.configuration')(1)
        RangoFacturacion = pool.get('account_invoice_facho.dian_rango_facturacion')
        
        from .exceptions import DianClientError
        from facho.fe.client import dian
        client = dian.DianClient(config.dian_fe_numeracion_username,
                                 config.dian_fe_numeracion_password)
        resp = client.request(
            dian.GetNumberingRange(
                config.dian_fe_NITObligadoFacturarElectronicamente,
                config.dian_fe_NITProveedorTecnologico,
                config.dian_fe_IdentificadorSoftware))

        if resp.CodigoOperacion != 'OK':
            raise DianClientError(resp.DescripcionOperacion)

        to_insert = []
        for rango_facturacion in resp.RangoFacturacion:
            to_insert.append({
                'NumeroResolucion': rango_facturacion.NumeroResolucion,
                'FechaResolucion': rango_facturacion.FechaResolucion,
                'Prefijo': rango_facturacion.Prefijo,
                'RangoInicial': rango_facturacion.RangoInicial,
                'RangoFinal': rango_facturacion.RangoFinal,
                'FechaVigenciaDesde': rango_facturacion.FechaVigenciaDesde,
                'FechaVigenciaHasta': rango_facturacion.FechaVigenciaHasta,
                'ClaveTecnica': rango_facturacion.ClaveTecnica,
            })
        RangoFinal.create(to_insert)
        return 'end'
